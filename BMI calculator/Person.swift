//
//  Person.swift
//  BMI calculator
//
//  Created by nguyen thanh vy on 29/03/2020.
//  Copyright © 2020 nguyen thanh vy. All rights reserved.
//

import UIKit

class Person {
    //MARK: Properties
    let minHeight :Double = 130
    let minWeight :Double = 40
    let name : String
    var weight : Double
    var height : Double
    var BMI : Double {
        get {
            let heightInMeters = Double(height)/100.0
            return Double(weight)/(heightInMeters*heightInMeters)
        }
    }
    var age : Int
    var profession: Array<String>
    let numberCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

    init?(name: String, weight: Double, height: Double, age: Int, profession: Array<String>) {
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        // Name does not contain number
        for number in numberCharacters{
            if name.contains(number) {
                return nil
            }
        }
        func setHeight(_ height: Double) {
            if (height >= minHeight) {
                self.height = height
            } else {
                self.height = minHeight
            }
        }
        func setWeight(_ weight: Double) {
            if (height >= minWeight) {
                self.weight = weight
            } else {
                self.weight = minWeight
            }
        }
            
        // The age should not be negative or very large
        guard (age > 0) && (age <= 150) else {
            return nil
        }
        // The number of professtion items should not be excess 5
        guard (profession.count < 5)  else {
            return nil
        }
        
        self.name = name
        self.weight = weight
        self.height = height
        self.age = age
        self.profession = profession
    }
    func changeAge(newAge: Int) {
        if (newAge > age){
            age = newAge
        }
    }
}
