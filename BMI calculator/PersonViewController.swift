//
//  PersonViewController.swift
//  BMI calculator
//
//  Created by nguyen thanh vy on 29/03/2020.
//  Copyright © 2020 nguyen thanh vy. All rights reserved.
//
import os.log
import UIKit

class PersonViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //MARK: Properties
    var userHeight : Double = 0
    var userWeight : Double = 0
    var userHeightInMetre : Double = 0
    var userBMI : Double = 0
    var heightData = (130...200).map { String($0) }
    var weightData = (40...140).map { String($0) }
    var person: Person?
  
    @IBOutlet weak var BMILabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var heightPickerView: UIPickerView!
    @IBOutlet weak var weightPickerView: UIPickerView!
    @IBOutlet weak var heightTextView: UITextField!
    @IBOutlet weak var weightTextView: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameTextField.delegate = self
        weightPickerView.dataSource = self
        weightPickerView.delegate = self
        heightPickerView.dataSource = self
        heightPickerView.delegate = self
    
    }

    //MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // Disable the Save button while editing.
        saveButton.isEnabled = false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        saveButton.isEnabled = true
    }

    //MARK: UIPickerViewDelegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var countrows : Int = self.heightData.count
        if pickerView == weightPickerView {
        countrows = self.weightData.count
        }
        return countrows
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == heightPickerView {
            self.heightTextView.text = self.heightData[row]
            userHeight = Double(self.heightData[row])!
        }
        else if pickerView == weightPickerView{
            self.weightTextView.text = self.weightData[row]
            userWeight = Double(self.weightData[row])!
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == heightPickerView {
               
               let titleRow = heightData[row]

                   return titleRow
                   
               }

               else if pickerView == weightPickerView{
                   let titleRow = weightData[row]
                   
                   return titleRow
               }
               
               return ""
        
    }
    @IBAction func calculateButtonClicked(_ sender: UIButton) {
        userHeightInMetre = userHeight/100
        userBMI = userWeight/(userHeightInMetre * userHeightInMetre)
        BMILabel.text = "Your BMI is: \(String(format: "%.1f", userBMI))"
        
    }
    //MARK: Navigation
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        let name = nameTextField.text ?? ""
        let weight = userWeight
        let height = userHeight
        let age : Int = 20
        let profession = ["teacher"]
        
        person = Person(name: name, weight: weight, height: height , age: age, profession: profession)
        
    }
    
    //MARK: Private Methods
    
}
