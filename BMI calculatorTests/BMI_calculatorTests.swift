//
//  BMI_calculatorTests.swift
//  BMI calculatorTests
//
//  Created by nguyen thanh vy on 29/03/2020.
//  Copyright © 2020 nguyen thanh vy. All rights reserved.
//

import XCTest
@testable import BMI_calculator

class BMI_calculatorTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    //MARK: Person Class Tests
    
    func testPersonInitializationSucceeds() {
        let person1 = Person(name: "Peter", weight: 50, height:160, age: 50, profession: ["teacher"])
        XCTAssertNotNil(person1)
    }
    func testPersonInitializationFails() {
        //negative age
        let person2 = Person(name: "Peter", weight: 50, height:160, age: -1, profession: ["teacher"])
        XCTAssertNil(person2)
        //very large age
        let person3 = Person(name: "Peter", weight: 50, height:160, age: 200, profession: ["teacher"])
        XCTAssertNil(person3)
        let person4 = Person(name: "Peter", weight: 50, height:160, age: 200, profession: ["teacher","teacher","teacher","teacher","singer","teacher"])
        XCTAssertNil(person4)
    }
    
    
}
